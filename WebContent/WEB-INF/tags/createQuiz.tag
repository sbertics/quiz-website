<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@tag import="project.User"%>
<%User user = (User) request.getSession().getAttribute("user");%>
<%
int RANDOM_ORDER = 1;
int ORDER = 0;
int SINGLE_PAGE = 0;
int MULTI_PAGE_WAIT = 1;
int MULTI_PAGE_IMMEDIATE = 2;
%>
<article>
<h3>Let's Create A Quiz</h3>
<div class="forms">
	<form action=CreateQuiz method="post">

		<table>
			<tr>
				<td align="right">Quiz Name</td>
				<td align="left"><input type="text" name="name">
				</td>
			</tr>
			<tr>
				<td align="right">Description</td>
				<td align="left"><textarea name='description' rows=6 cols=40> </textarea>
				</td>
			</tr>
			<tr>
				<td align="right">Order Type</td>
				<td align="left"><select name="order">
						<option value="0">Ordered</option>
						<option value="1">Random</option>
						</select>
				</td>
			</tr>
			<tr>
				<td align="right">Display Type</td>
				<td align="left"><select name="pageType">
						<option value="0">Single Page</option>
						<option value="1">MultiPage</option>
						<option value="2">MultiPage - Immediate feedback</option>
						</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="right"><input type="submit"
					value="Create"></td>
			</tr>
		</table>
	</form>
</article>

