<%@ tag import="quiz.*,project.*" %>
<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%
	User user = (User) request.getSession().getAttribute("user");
	Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
	if (user != null && quiz != null) {
		if (quiz.getCreatorID() == user.getUserID()) {
			out.println("<form action='edit-quiz.jsp' method='get'><input type='submit' value='Edit Quiz'></form>");
		}
	}
%>