<%@ tag language="java" pageEncoding="ISO-8859-1" import="quiz.*"%>
<% Quiz quiz = (Quiz) request.getSession().getAttribute("quiz"); %>
<ol>
<% for (int i = 0; i < quiz.getNumQuestions(); i++) { %>
	<li>
	<%= quiz.getQuestion(i).displayQuestionAndAnswers() %>
		<input type='submit' name='<%=i%>' value='Edit'>
		<input type='submit' name='<%=i%>' value='Delete'>
	</li>
<% } %>
</ol>