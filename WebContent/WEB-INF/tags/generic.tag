<%@tag description="Overall Page template" language="java" pageEncoding="ISO-8859-1" import="project.*"%>
<%@attribute name="head" fragment="true"%>
<%User user = (User) request.getSession().getAttribute("user"); %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="style.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<jsp:invoke fragment="head" />
</head>
<body>
	<div>
		<header>
			<div id="empty_div"></div>
			<ul class="h">
				<li class="h"><a class="h" href="home.jsp"><%= user.getUsername() %></a></li>
				<li class="h"><a class="h" href="user-friends.jsp">Friends</a></li>
				<li class="h"><a class="h" href="users.jsp">Users</a></li>
				<li class="h"><a class="h" href="user-mail.jsp">Mail</a></li>
				<li class="h"><a class="h" href="quizzes.jsp">Quizzes</a></li>
				<li class="h"><a class="h" href="LogoutServlet">Log Out</a></li>
			</ul>
		</header>
	</div>
	<jsp:doBody />
</body>
</html>

