<%@ tag language="java" pageEncoding="ISO-8859-1"%>

<% String message = (String) request.getSession().getAttribute("message"); %>
<% request.getSession().setAttribute("message", ""); %>

<%
	if (message != null && !message.equals("")) {
		out.println("<script>");		
		out.println("alert(\"" + message + "\");");
		out.println("</script>");
	}
%>