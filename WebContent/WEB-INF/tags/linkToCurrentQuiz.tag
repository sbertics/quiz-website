<%@ tag language="java" pageEncoding="ISO-8859-1" import="quiz.Quiz"%>
<% Quiz quiz = (Quiz) request.getSession().getAttribute("quiz"); %>
<form action="quiz.jsp" method="get">
	<input type="hidden" name="id" value="<%=quiz.getQuizID()%>">
	<input type="submit" value="Go to '<%=quiz.getName()%>' Homepage">
</form>