<%@ tag language="java" pageEncoding="ISO-8859-1" import="quiz.*,project.*"%>
<%
	try {
		Database db = (Database) request.getServletContext().getAttribute("database");
		Integer quizID = Integer.parseInt((String)request.getParameter("id"));
		Quiz quiz = Quiz.loadQuiz(db, quizID);
		request.getSession().setAttribute("quiz",quiz);
	} catch (Exception ignored) {}
%>