<%@ tag language="java" pageEncoding="ISO-8859-1" import="quiz.*,java.util.concurrent.TimeUnit"%>
<% Quiz quiz = (Quiz) request.getSession().getAttribute("quiz"); %>
<%
	long millis = quiz.getEndTime() - quiz.getStartTime();
	long hrs = (millis/3600000) % 24;
	long mins = (millis/60000) % 60;
	long secs = (millis/1000) % 60;
	millis %= 1000;
	
	if (hrs > 0) {
		out.println("" + hrs + " Hours " + mins + " Minutes " + secs + "." + millis + " Seconds");
	} else {
		out.println("" + mins + " Minutes " + secs + "." + millis + " Seconds");
	}
%>