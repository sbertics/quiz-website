<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@tag import="quiz.Quiz, project.*,java.sql.*" %>
<% Database db = (Database) request.getServletContext().getAttribute("database"); %>  
<h3>Recently Created Quizzes Worldwide</h3>
<%= Quiz.getRecentlyCreatedQuizzes(db, Quiz.DEFAULT_NUM_QUIZZES_TO_DISPLAY) %>
