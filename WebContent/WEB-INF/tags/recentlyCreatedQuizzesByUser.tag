<%@ tag language="java" pageEncoding="ISO-8859-1"%>
<%@tag import="quiz.Quiz, project.*,java.sql.*" %>
<% Database db = (Database) request.getServletContext().getAttribute("database"); %>
<% User user = (User) request.getSession().getAttribute("user"); %>
<h3>Your Recently Created Quizzes</h3>
<%= Quiz.getUsersRecentlyCreatedQuizzes(db, user.getUserID(), Quiz.DEFAULT_NUM_QUIZZES_TO_DISPLAY) %>