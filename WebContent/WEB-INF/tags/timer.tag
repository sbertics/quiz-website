<%@ tag language="java" pageEncoding="ISO-8859-1" import="quiz.*"%>
<% Quiz quiz = (Quiz) request.getSession().getAttribute("quiz"); %>
<script type="text/javascript">

	function formatTime(time) {
		var h = time.getUTCHours();
		var m = time.getUTCMinutes();
		var s = time.getUTCSeconds();
		var tenths = Math.round(time.getTime()/100) % 10;
		var hours = "" + h + " hrs ";
		var minutes = "" + m + " min ";
		var seconds = "" + s + "." + tenths + " sec";
		if (h > 0) {
			return hours + minutes + seconds;
		} else if (m > 0) {
			return minutes + seconds;
		} else {
			return seconds;
		}
	}

	function clockTime() {
		var time = new Date();
		var curDate = new Date();
		var diff = curDate.getTime() - <%=quiz.getStartTime()%>;
		time.setTime(diff);
		document.getElementById("timer").innerHTML = "Time: " + formatTime(time);
	}
	
	setInterval(function(){clockTime();},100);
</script>
<noscript>Error: Javascript is required for the timer.</noscript>