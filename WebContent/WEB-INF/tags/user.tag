<%@ tag language="java" pageEncoding="ISO-8859-1" import="project.*"%>

<% User user = (User) request.getSession().getAttribute("user"); %>
<% int userId = Integer.parseInt((String)request.getParameter("id")); %> 
<% if (user.getUserID() == userId) response.sendRedirect("home.jsp"); %>
<article>
	<h3><%= user.getUsername() %></h3>
	<%=user.getFriendButton(userId) %>
	<br />
	 <%=user.getSendMessageForm(userId) %>
	<br />
	<%=user.getChallengeForm(userId) %>
</article>