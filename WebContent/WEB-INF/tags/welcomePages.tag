<%@tag description="welcome template" pageEncoding="UTF-8" language="java"%>
<%@attribute name="head" fragment="true" %>
<%@tag import="project.User" %> 
<%
			try{
				User user = (User) request.getSession().getAttribute("user");
				if(user.getUserID() != -1){
					response.sendRedirect("home.jsp");
				}
			}catch(Exception ignored){}; 
		%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link href="welcome.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type"
			content="text/html; charset=ISO-8859-1">
			<jsp:invoke fragment="head" />
	</head>
	<body>
	<div id="body">
		<jsp:doBody />
	</div>
	</body>
</html>

