<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:handleJavascriptMessage />
<t:welcomePages>
	<jsp:attribute name="head">
	<title>New User</title>
	</jsp:attribute>
	<jsp:body>
		<div class="topRight"><a href="login.jsp"><button>Login</button></a></div>
		<div class="forms">	
			<form action="CreateNewUserServlet" method="post">
				<fieldset>
					<legend>Create User</legend>
					<table>
						<tr>
							<td align="right">User Name</td>
							<td align="left"><input type="text" name="username"> </td>
						</tr>
						<tr>
							<td align="right">Password</td>
							 <td align="left"><input type="password" name="password"></td>
						</tr>
						<tr>
							<td  colspan="2" align="right"><input type="submit" value="Create"></td>
						</tr>
					</table>
				</fieldset>
			</form>
		</div>
    </jsp:body>
</t:welcomePages>
