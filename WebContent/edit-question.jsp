<%@page import="quiz.*,project.*"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:handleJavascriptMessage />
<t:generic>
	<jsp:attribute name="head">
		<title>Edit Question</title>
	</jsp:attribute>
	<jsp:body>
		<article>
			<form action='SaveQuizQuestionServlet' method='post'>
				<t:editQuestion />
				<input type='submit' value='Save Question'>
			</form>
		</article>
	</jsp:body>
</t:generic>