<%@page import="quiz.*,project.*"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
    
<t:handleJavascriptMessage />
<t:generic>
	<jsp:attribute name="head">
		<title>Edit Quiz Data</title>
	</jsp:attribute>
	<jsp:body>
		<article>
			<form action='EditQuizDataServlet' method='post'>
				<t:editQuizData />
				<input type="submit" value="Save Changes">
			</form>
		</article>
	</jsp:body>
</t:generic>