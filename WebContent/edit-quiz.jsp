<%@page import="quiz.*,project.*"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:handleJavascriptMessage />
<t:generic>

	<jsp:attribute name="head">
		<title>Edit Quiz</title>
	</jsp:attribute>

	<jsp:body>
		<article>
		
			<h1>Edit Quiz</h1>
			<h2>Quiz Summary</h2>
			<form action='edit-quiz-data.jsp' method='get'>
				<t:quizSummary />
				<input type='submit' value='Edit Quiz Data'>
			</form>
			<form action='DeleteQuizServlet' method='post'>
				<input type='submit' value='Delete Quiz'>
			</form>
		
			<h2>Quiz Questions</h2>
			<form action='EditQuizQuestionServlet' method='post'>
				<t:editQuizQuestions />
			</form>
		
			<form action='CreateNewQuizQuestionServlet' method='post'>
				<t:createNewQuestionSelectType />
				<input type='submit' value='Add Question'>
			</form>
			<br>
			<t:linkToCurrentQuiz/>
		</article>
	</jsp:body>
</t:generic>