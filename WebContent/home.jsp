<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:handleJavascriptMessage />
<t:generic>
	<jsp:attribute name="head">
	<title>The Q Home</title>
	</jsp:attribute>
	<jsp:body>
		<article>
			<a href="CreateNewQuizServlet">
				<button>Create A Quiz!</button>
			</a>
			<t:recentlyCreatedQuizzesByUser />	
			<t:recentlyCreatedQuizzes />
		</article>
	</jsp:body>
</t:generic>


