<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:handleJavascriptMessage />
<t:welcomePages>
	<jsp:attribute name="head">
	<title>The Q</title>
	</jsp:attribute>
	<jsp:body>
		<div class="topRight"><a href="create-user.jsp"><button>New User?</button></a></div>
		<div class="forms">	
			<form action=LoginServlet method="post">
				<fieldset>
					<legend>Log In</legend>
					<table>
						<tr>
							<td align="right">User Name</td>
							<td align="left"><input type="text" name="username"> </td>
						</tr>
						<tr>
							<td align="right">Password</td>
							 <td align="left"><input type="password" name="password"></td>
						</tr>
						<tr>
							<td  colspan="2" align="right"><input type="submit" value="Login"></td>
						</tr>
					</table>
				</fieldset>
			</form>
		</div>
    </jsp:body>
</t:welcomePages>

