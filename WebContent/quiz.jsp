<%@page import="quiz.*,project.*"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<t:handleJavascriptMessage />
<t:loadQuizFromID />
<t:generic>
	<jsp:attribute name="head">
		<title>Quiz</title>
	</jsp:attribute>
	<jsp:body>
		<article>
			<h2>Quiz Summary</h2>
			<t:quizSummary/>
			<t:editQuizButton/>
			<form action='StartQuizServlet' method='post'>
				<input type='submit' value='Start Quiz!'>
			</form>
		</article>
	</jsp:body>
</t:generic>