<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:generic>
	<jsp:attribute name="head">
	<title>Quizzes</title>
	</jsp:attribute>
	<jsp:body>
		<article>
			<t:quizzes />
		</article>
	</jsp:body>	
</t:generic>