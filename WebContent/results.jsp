<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:handleJavascriptMessage />
<t:generic>
	<jsp:attribute name="head">
		<title>Quiz Results</title>
	</jsp:attribute>
	<jsp:body>
		<article>
			<h1>Quiz Results</h1>
			<p>You scored <t:quizScore/> on the quiz!</p>
			<p>Time: <t:quizTime/></p>
			<t:quizResultsBreakdown/>
			<t:editQuizButton/>
			<t:linkToCurrentQuiz/>
		</article>
	</jsp:body>
</t:generic>
