<%@page import="quiz.*,project.*"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:handleJavascriptMessage />
<t:generic>
	<jsp:attribute name="head">
	<title>Go</title>
	</jsp:attribute>
	<jsp:body>
		<article>
			<h2>Take Quiz</h2>
			<div id="timer"></div>
			<t:timer/>
			<form action='SubmitQuizServlet' method='post'>
				<t:takeQuiz />
				<input type='submit' value='Submit'>
			</form>
		</article>
	</jsp:body>
</t:generic>
