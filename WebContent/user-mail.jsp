<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:generic>
	<jsp:attribute name="head">
	<title>Mail</title>
	</jsp:attribute>
	<jsp:body>
		<t:friendRequests />
		<t:challenges />
		<t:messages />
	</jsp:body>
</t:generic>
