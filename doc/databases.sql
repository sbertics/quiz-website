USE c_cs108_sbertics;

-- QUIZ TABLE:
-- quizID - Automatically assigned identifier for the quiz
-- creatorID - The userID of the creator of the quiz
-- name - The title of the quiz limited to 32 characters
-- category - The category of the quiz
-- description - The description of the quiz
-- questionOrderScheme - NUMBERED (0) or RANDOM (1)
-- pageDisplayFormat - SINGLE_PAGE (0), MULTI_PAGE (1), MULTI_PAGE_IMMEDIATE (2)
CREATE TABLE IF NOT EXISTS quizzes (
	quizID INT NOT NULL AUTO_INCREMENT,
	creatorID INT NOT NULL,
	name VARCHAR(32) NOT NULL,
	category VARCHAR(32),
	description TEXT,
	questionOrderScheme INT,
	pageDisplayFormat INT,
	createTime TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (quizID)
);

-- QUESTIONS TABLE:
-- questionID - Automatically assigned identifier for the question
-- quizID - The quiz that this question is associated with
-- questionType - Determines which class is used to create and manage this question
-- questionOrder - If multiple question exist for the same quiz, sort them by order before printing them out
CREATE TABLE IF NOT EXISTS questions (
	questionID INT NOT NULL AUTO_INCREMENT,
	quizID INT NOT NULL,
	questionType VARCHAR(32),
	questionOrder INT,
	PRIMARY KEY (questionID)
);

-- ELEMENT TABLE:
-- elementID - Automatically assigned identifier for the question element
-- elementType - Either 'QUERY', 'OPTION', or 'ANSWER'
-- questionID - The question that this element is associated with
-- elementStr - The textual content of this element
-- elementOrder - If multiple elements exist for the same question, sort them by order before printing them out
-- isCorrect - Boolean for correct or incorrect (may or may not be used depending on the type of the element)
CREATE TABLE IF NOT EXISTS elements (
	elementID INT NOT NULL AUTO_INCREMENT,
	elementType VARCHAR(32) NOT NULL,
	questionID INT NOT NULL,
	elementStr TEXT,
	elementOrder INT,
	isCorrect TINYINT(1),
	PRIMARY KEY (elementID)
);

-- USERS TABLE:
-- userID Name PasswordHash
CREATE TABLE IF NOT EXISTS users (
	userID INT NOT NULL AUTO_INCREMENT,
	username VARCHAR(32) NOT NULL,
	passwordHash VARCHAR(40) NOT NULL,
	PRIMARY KEY (userID)
);

-- FRIENDS TABLE:
-- UserID FriendID
-- (Added two entries for every user-friend and friend-user pair)
CREATE TABLE IF NOT EXISTS friends (
	userID INT NOT NULL,
	friendID INT NOT NULL
);

-- MAIL TABLE:
-- SenderID: userID of sender
-- ReceiverID: userID of receiver
-- Note: text of the note, or name of the challenge quiz
CREATE TABLE IF NOT EXISTS mail (
	senderID INT NOT NULL,
	receiverID INT NOT NULL,
	messageText TEXT
);

-- CHALLENGE TABLE:
-- SenderID: userID of sender
-- ReceiverID: userID of receiver
-- quizID: the quiz that is being challenged
CREATE TABLE IF NOT EXISTS challenges (
	senderID INT NOT NULL,
	receiverID INT NOT NULL,
	quizID INT NOT NULL
);
