package project;

import java.sql.*;

public class Database {

	private Connection con;	

	public Database(){
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection("jdbc:mysql://"
					+ MyDBInfo.MYSQL_DATABASE_SERVER, MyDBInfo.MYSQL_USERNAME,
					MyDBInfo.MYSQL_PASSWORD);
		} catch (Exception ignored) {}
	}
	
	/**
	 * Returns the database connection
	 */
	public Connection getConnection() { return con; }
	
	/**
	 * Update the mysql table for the given mysql command.
	 * @return auto generated ID if one was used or -1
	 */
	public int update(String update) {
		int id = -1;
		try {
			Statement stmt = con.createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			stmt.executeUpdate(update, Statement.RETURN_GENERATED_KEYS);
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getInt(1);
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
		return id;
	}
	
	/**
	 * Close the connection to the database when the server shuts down.
	 */
	public void closeConnection(){
		try {
			con.close();
		} catch (SQLException ignored) {}
	}
}
