package project;

import java.security.MessageDigest;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import project.Database;
import quiz.QuizData;

import com.sun.tools.javac.util.Pair;

public class User {
	
	//////////////////////////////
	//  STATIC CLASS FUNCTIONS  //
	//////////////////////////////
	
	private static final int MAX_USERNAME_LEN = 32;
	
	/**
	 * Returns an unordered HTML list of all users on the site
	 * with links to their pages.
	 */
	public static String getListAllUsersHTML(Database db) {
		ArrayList<User> users = User.getAllUsers(db);
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		for (User u : users) {
			sb.append("<li><a href='user.jsp?id=");
			sb.append(u.getUserID());
			sb.append("'>");
			sb.append(u.getUsername());
			sb.append("</a></li>");
		}
		sb.append("</ul>");
		return sb.toString();
	}
	
	/**
	 * Returns a list of all the users in the database as instantiated
	 * user objects
	 */
	public static ArrayList<User> getAllUsers(Database db) {
		ArrayList<User> users = new ArrayList<User>();
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			String query = "SELECT * FROM users";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				User u = new User(db);
				u.userID = rs.getInt("userID");
				u.username = rs.getString("username");
				users.add(u);
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
		return users;
	}
	
	/**
	 * Creates a new user and adds it to the database.  Returns the user
	 * object on success or null on failure.
	 */
	public static User createNewUser(Database db, String username, String password) {
		if (User.isUsernameInUse(db, username)) return null;
		if (username.length() > MAX_USERNAME_LEN) return null;
		String hash = hexToString(generateHash(password));
		User new_user = new User(db);
		new_user.userID = db.update("INSERT INTO users VALUES (NULL,'" + username + "','" + hash + "')");
		new_user.username = username;
		if (new_user.userID == -1) return null;
		return new_user;
	}
	
	/**
	 * Returns true if the username already exists in the database, false otherwise
	 */
	private static boolean isUsernameInUse(Database db, String username) {
		boolean exists = false;
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			String query = "SELECT * FROM users WHERE username = '" + username + "'";
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) exists = true;
			rs.close();
			stmt.close();
		} catch (Exception ignored) { }
		return exists;
	}
	
	/**
	 * Given the username/password combo, this function returns a new user object
	 * on successful login or null on failure.
	 */
	public static User createUser(Database db, String username, String password) {
		int userID = User.checkUsernamePassword(db, username, password);
		if (userID == -1) return null;
		User new_user = new User(db);
		new_user.userID = userID;
		new_user.username = username;
		return new_user;
	}
	
	/**
	 * Contacts the database to instantiate a user object given the userID.
	 */
	public static User createUser(Database db, int userID) {
		User user = null;
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			String query = "SELECT * FROM users WHERE userID=" + userID;
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {
				user = new User(db);
				user.userID = userID;
				user.username = rs.getString("username");
			}
		} catch (Exception ignored) {}
		return user;
	}
	
	/**
	 * Returns the positive integer userID on success or -1 on failure.
	 */
	private static int checkUsernamePassword(Database db, String username, String password) {
		if (username.length() > MAX_USERNAME_LEN) return -1;
		int userID = -1;
		String hash = hexToString(generateHash(password));
		String query = "SELECT * FROM users WHERE username = '" + username + "' AND passwordHash = '" + hash + "'";
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {
				userID = rs.getInt("userID");
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) { }
		return userID;
	}
	
	/**
	 * Given a byte[] array, produces a hex String, such as "234a6f"
	 * with two chars for each byte in the array.
	 */
	private static String hexToString(byte[] bytes) {
		StringBuffer buff = new StringBuffer();
		for (int i = 0; i < bytes.length; i++) {
			int val = bytes[i];
			val = val & 0xff;  // remove higher bits, sign
			if (val < 16) buff.append('0'); // leading 0
			buff.append(Integer.toString(val, 16));
		}
		return buff.toString();
	}

	/**
	 * Taken from cracker, used to generate our hash
	 */
	private static byte[] generateHash(String pass){
		byte[] bytes = pass.getBytes();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA");
			md.update(bytes);
			bytes = md.digest();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bytes;
	}

	
	//////////////////////////////
	// INSTANCE CLASS FUNCTIONS //
	//////////////////////////////
	
	private Database db;
	private int userID;
	private String username;

	private User(Database db) {
		this.db = db;
	}

	// GETTERS
	public int getUserID() { return userID; }
	public String getUsername() { return username; }
	public String getLinkToUserHTML() { return "<a href='user.jsp?id=" + userID + "'>" + username + "</a>"; }

	//////////////////////////////
	//   FRIEND REQUEST STUFF   //
	//////////////////////////////
	
	public static final int NOT_FRIENDS = 0;  
	public static final int MY_FRIEND = 1;
	public static final int THEIR_FRIEND = 2;  
	public static final int FRIENDS = 3; 
	
	/*goes into the db and checks the friendship quality of two people
	 * returns int indicating quality of friendship
	 */
	private int friendQuality(int friendID) {
		int quality = NOT_FRIENDS;
		try {
			boolean amITheirFriend = false;
			boolean areTheyMyFriend = false;
			String query = "SELECT friendID FROM friends where userID = "+ userID+" AND friendID = " + friendID + ";";
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				amITheirFriend = true;
			}
			rs.close();
			query = "SELECT friendID FROM friends where userID = "+ friendID+" AND friendID = " + userID + ";";
			rs = stmt.executeQuery(query);
			while(rs.next()){
				areTheyMyFriend= true;
			}
			rs.close();
			stmt.close();
			if(amITheirFriend && areTheyMyFriend){
				quality = FRIENDS;
			}else if(amITheirFriend && !areTheyMyFriend){
				quality = MY_FRIEND;
			}else if(!amITheirFriend && areTheyMyFriend){
				quality = THEIR_FRIEND;
			}
		} catch (Exception ignored) {}
		return quality;
	}

	/*
	 * Goes into db and adds this friend
	 */
	public  void createFriendRequest(int requesteeID){
		String update = "INSERT INTO friends VALUES(" + userID + ", " + requesteeID+ ")";
		db.update(update);
	}

	/*
	 * Goes into database and deletes both friends as friends
	 */
	public  void unFriend(int requesteeID){
		String update = "DELETE FROM friends WHERE userID = " + userID +" AND friendID = " + requesteeID;
		db.update(update);
		update = "DELETE FROM friends WHERE userID = " + requesteeID +" AND friendID = " + userID;
		db.update(update);
	}

	/*Goes in and creates html for a addFriend Button*/
	public String getFriendButton(int userPage){
		StringBuilder sb = new StringBuilder();
		String servlet;
		String submit;
		int quality = friendQuality(userPage);
		if(quality == FRIENDS){
			servlet = "RemoveFriendServlet";
			submit = "Remove Friend";
		}else if(quality == THEIR_FRIEND || quality == NOT_FRIENDS){
			servlet = "AddFriendServlet";
			submit = "Add Friend";
		}else {
			return "";
		}
		sb.append("<form action=");
		sb.append(servlet);
		sb.append(" method='POST'> ");
		sb.append("<input type='hidden' name='requesteeID' value='");
		sb.append(userPage);
		sb.append("'>");
		sb.append("<input type='submit' value='");
		sb.append(submit);
		sb.append("'> </form>");
		return sb.toString();
	}



	/*Goes through and finds all of your friends */
	private ArrayList<Integer> friends(){
		ArrayList<Integer> f = new ArrayList<Integer>();
		try {
			String query = "SELECT friendID FROM friends where userID = "+ userID + ";";
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				f.add(rs.getInt("friendID"));
			}
			rs.close();
			rs.close();
			stmt.close();

		} catch (Exception ignored) {}
		return f;
	}

	/*Goes in and creates html for all of the friends*/
	public String getFriends(){
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		ArrayList<Integer> f = friends();
		for (int stranger : f) {
			if (friendQuality(stranger) == FRIENDS) {
				sb.append("<li>");
				sb.append("<a href='user.jsp?id=");
				sb.append(stranger);
				sb.append("'>");
				sb.append(User.createUser(db, stranger).getUsername());
				sb.append("</a></li>");
			}
		}
		sb.append("</ul>");
		return sb.toString();
	}
	
	public String getNotifications(){
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		sb.append("<li>");
		ArrayList<Integer> f = getRequests();
		if(f.isEmpty()){
			sb.append("No new friend Requests");
		}else{
			sb.append("You have " + f.size() + " requests waiting for you.");
		}
		sb.append("</li>");
		sb.append("<li>");
		ArrayList<Pair<Integer, Integer> > challenges = grabChallengers();
		if(challenges.isEmpty()){
			sb.append("There are no challenges waiting.");
		}else{
			sb.append("You have " + challenges.size() + " challenges waiting for you.");
		}
		sb.append("</li>");
		sb.append("<li>");
		Map<Integer, String>  notes = getNoteMap();
		if(notes.isEmpty()){
			sb.append("There are no messages.");
		}else{
			sb.append("You have " + notes.size() + " messages waiting for you.");
		}
		sb.append("</li>");
		sb.append("</ul>");
		return sb.toString();
	}

	/*
	 * Goes through and find all the friend requests
	 */
	private ArrayList<Integer> getRequests(){
		ArrayList<Integer> f = new ArrayList<Integer>();
		try {
			String query = "SELECT userID FROM friends where friendID = "+ userID + ";";
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				f.add(rs.getInt("userID"));
			}
			rs.close();
			rs.close();
			stmt.close();

		} catch (Exception ignored) {}
		for(int i = f.size() - 1; i >= 0; i--){
			if(friendQuality(f.get(i)) != THEIR_FRIEND){
				f.remove(i);
			}
		}
		return f;
	}

	/*Goes in and creates html for all of the friendRequests */	
	public String getFriendRequests(){
		StringBuilder sb = new StringBuilder();
		sb.append("<table > <thead> <tr><td>Requester</td> <td></td> </tr> </thead> <tbody>");
		ArrayList<Integer> requests = getRequests();
		if(requests.isEmpty()) return "Looks like there are no friend requests";
		for(int i = 0; i < requests.size(); i++){
			sb.append("<tr> <td><a href='user.jsp?id=");
			sb.append(requests.get(i));
			sb.append("'>");
			sb.append(User.createUser(db, requests.get(i)).getUsername());
			sb.append("<td> <form action=FriendRequestServlet method='POST'> <input type='hidden' name='requesteeID'  value='");
			sb.append(requests.get(i));
			sb.append("'> <input type='radio' name='request' value='accept'>Accept <input type='radio' name='request' value='reject'>Reject <input type='submit' value='submit'> </form> </td> </tr>");
		}
		sb.append("</tbody> </table>");
		return sb.toString();
	}

	/*Sends a message from one user to another!*/
	public  void sendMessage(int userPage, String message){
		String update = "INSERT INTO mail VALUES(" + userID +", " + userPage + ", '" + message +"');";
		db.update(update);
	}

	/*Generates html for sendmessage form*/
	public String getSendMessageForm(int userPage){
		StringBuilder sb = new StringBuilder();
		if(friendQuality(userPage) == FRIENDS){
			sb.append("<form action=");
			sb.append("SendMessageServlet");
			sb.append(" method='POST'> ");
			sb.append("<input type='hidden' name='requesteeID' value='");
			sb.append(userPage);
			sb.append("'>");
			sb.append("<textarea name='message' rows=6 cols=40> </textarea>");
			sb.append("<br /><input type='submit' value='");
			sb.append("Send Message");
			sb.append("'> </form>");
		}
		return sb.toString();
	}

	/* 
	 * Go into the database and find all the notes sent for this particular userID
	 */
	private  Map<Integer, String> getNoteMap(){
		Map<Integer, String> notes = new HashMap<Integer, String>();
		try {
			String query = "SELECT * FROM mail WHERE receiverID = " + userID+ ";";
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				int sender = rs.getInt("senderID");
				String note = rs.getString("messageText");
				notes.put(sender, note);
			}
			rs.close();
			rs.close();
			stmt.close();

		} catch (Exception ignored) {}
		return notes;
	}

	/*Goes in and creates html for all of the notes */
	public String getNotes(){
		StringBuilder sb = new StringBuilder();
		sb.append("<table > <thead> <tr> <td>User</td> <td>Message</td> </tr> </thead> <tbody>");
		Map<Integer, String> notes = getNoteMap();
		if (notes.isEmpty()) return "There doesn't seem to be any messages";
		for (Map.Entry<Integer, String> entry : notes.entrySet()) {
			sb.append("<tr> <td><a href='user.jsp?id=");
			sb.append(entry.getKey());
			sb.append("'>");
			sb.append(User.createUser(db, entry.getKey()).getUsername());
			sb.append("</a> <td>");
			sb.append(entry.getValue());
			sb.append("</td> </tr>");
		}
		sb.append("</tbody> </table>");
		return sb.toString();
	}

	/* Goes into the database and grabs all of those that wish to challenge you */
	private ArrayList<Pair<Integer, Integer> > grabChallengers(){
		ArrayList<Pair<Integer, Integer> > challengers = new ArrayList<Pair<Integer, Integer> >();
		try {
			String query = "SELECT * FROM challenges WHERE receiverID="+ userID;
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			while(rs.next()){
				int senderID = rs.getInt("senderID");
				int quizID = rs.getInt("quizID");
				Pair<Integer, Integer> p = new Pair<Integer, Integer>(senderID, quizID);
				challengers.add(p);
			}
			rs.close();
			rs.close();
			stmt.close();

		} catch (Exception ignored) {}
		return challengers;
	}
	/*Goes in and creates html for all of the challenges */
	public String getChallenges(){
		StringBuilder sb = new StringBuilder();
		ArrayList<Pair<Integer, Integer> > challengers = grabChallengers();
		if(challengers.isEmpty())return "You don't have any challenges";
		sb.append("<table > <thead> <tr><td>Challenger</td> <td>Quiz</td></tr> </thead> <tbody>");	

		for(int i = 0; i < challengers.size(); i++){
			Pair<Integer, Integer> challenge = challengers.get(i);
			int senderID = challenge.fst;
			int quizID = challenge.snd;
			sb.append("<tr> <td><a href='user.jsp?id=");
			sb.append(senderID);
			sb.append("'>");
			sb.append(User.createUser(db, senderID).getUsername());
			sb.append("</a> </td><td><a href='quiz.jsp?id=");
			sb.append(quizID);
			sb.append("'>");
			sb.append(QuizData.getAllQuizzes(db, "SELECT * FROM quizzes WHERE quizID=" + quizID).get(0).getName());
			sb.append("</a></td><td></tr>");
		}
		sb.append("</tbody> </table>");
		return sb.toString();
	}

	/*
	 * Sending a challenge!
	 */
	public void sendChallenge(int userPage, int quizID){
		String update = "INSERT INTO challenges VALUES(" + userID + ", " + userPage + ", " + quizID +")";
		db.update(update);
	}

	/*Goes in and creates html for a challenge*/
	public String getChallengeForm(int userPage){
		StringBuilder sb = new StringBuilder();
		String query = "SELECT * from quizzes";
		ArrayList<QuizData> quizzes = QuizData.getAllQuizzes(db, query);
		if(quizzes.isEmpty()) return "";
		if(friendQuality(userPage) == FRIENDS){
			sb.append("<form action=");
			sb.append("'SendChallengeServlet'");
			sb.append(" method='post'> <input type='hidden' name='requesteeID' value='");
			sb.append(userPage);
			sb.append("'>");
			sb.append("<select name='challengeID'>");
			for(int i = 0; i < quizzes.size(); i++){
				sb.append("<option value='");
				sb.append(quizzes.get(i).getQuizID());
				sb.append("'>");
				sb.append(quizzes.get(i).getName());
				sb.append("</option>");
			}
			sb.append("</select><br /><input type='submit' value='");
			sb.append("Send Challenge");
			sb.append("'> </form>");
		}
		return sb.toString();
	}

	public String getFriendNotifications(){
		boolean areFriendsActive = false;
		StringBuilder sb = new StringBuilder();
		ArrayList<Integer> f = friends();
		sb.append("<ul>");
		for(int i = 0; i < f.size(); i++){
			if(friendQuality(f.get(i)) == FRIENDS){
				int friendID = f.get(i);
				String query = "SELECT quizID from quizzes WHERE creatorID="+ friendID;
				ArrayList<QuizData> allQuizzes = QuizData.getAllQuizzes(db, query);
				for (QuizData cur : allQuizzes) {
					areFriendsActive = true;
					sb.append("<li>");
					sb.append("<a href='user.jsp?id=");
					sb.append(f.get(i));
					sb.append("'>");
					sb.append(User.createUser(db, f.get(i)).getUsername());
					sb.append("</a>");
					sb.append(" created ");
					sb.append("<a href='quiz.jsp?id=");
					sb.append(cur.getQuizID());
					sb.append("'>");
					sb.append(cur.getName());
					sb.append("</a>");
					sb.append("</li>");
				}
			}
		}
		sb.append("</ul>");
		if (areFriendsActive) {
			return sb.toString();
		} else {
			return "Your friends don't seem to be doing much";
		}
	}
}