package quiz;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;

import project.MyDBInfo;

/**
 * The Fill In The Blank question presents the user with a sentence or sequence
 * of phrases containing a blank text field that the user can type into.
 * @author scott
 */
public class FillInTheBlankQuestion extends QuizQuestion {

	public static final String QUESTION_TYPE = "FILL IN THE BLANK";
	
	private String preText, postText;
	private String defaultText;
	private String preferredResponse;
	private HashSet<String> responses;
	
	protected FillInTheBlankQuestion() {
		this.preText = "";
		this.postText = "";
		this.defaultText = "";
		this.preferredResponse = "";
		this.responses = new HashSet<String>();
	}
	
	@Override
	protected void downloadElements() {
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery("SELECT * FROM elements where questionID=" + questionID);
			while (rs.next()) {
				String data = rs.getString("elementStr");
				String type = rs.getString("elementType");
				int order = rs.getInt("elementOrder");
				
				// Parse the various types of question elements differently
				if (type.equals("QUERY")) {
					if (order == 0) preText = data;
					else postText = data;
				} else if (type.equals("OPTION")) {
					defaultText = data;
				} else if (type.equals("ANSWER")) {
					int pref = rs.getInt("isCorrect");
					if (pref == 1) preferredResponse = data;
					else responses.add(data);
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
	}
	
	@Override
	public String displayQuestion() {
		return preText + " __________ " + postText;
	}
	
	@Override
	public String displayQuestionAndAnswers() {
		StringBuilder sb = new StringBuilder();
		sb.append("Question: ");
		sb.append(displayQuestion());
		sb.append("<br>Default Response Text: ");
		sb.append(defaultText);
		sb.append("<br>Preferred Response: ");
		sb.append(preferredResponse);
		sb.append("<br>Other Responses:<br><ul>");
		for (String s : responses) {
			sb.append("<li>");
			sb.append(s);
			sb.append("</li>");
		}
		sb.append("</ul>");
		return sb.toString();
	}
	
	@Override
	public String getQuestionHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append(preText);
		sb.append("<input type='text' name='");
		sb.append(questionID);
		sb.append("' placeholder='");
		sb.append(defaultText);
		sb.append("'>");
		sb.append(postText);
		return sb.toString();
	}

	@Override
	public String getEditQuestionHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append("Text Before the Fill-In: <input type='text' name='preText' value='");
		sb.append(preText);
		sb.append("'><br>");
		sb.append("Default Response Text: <input type='text' name='defaultText' value='");
		sb.append(defaultText);
		sb.append("'><br>");
		sb.append("Text After the Fill-In: <input type='text' name='postText' value='");
		sb.append(postText);
		sb.append("'><br>");
		sb.append("Preferred Response: <input type='text' name='preferredResponse' value='");
		sb.append(preferredResponse);
		sb.append("'><br>");
		sb.append("Other Responses (put each response on its own line):<br><textarea rows='10' cols='40' name='responses'>");
		for (String s : responses) {
			sb.append(s);
			sb.append("\n");
		}
		sb.append("</textarea>");
		return sb.toString();
	}
	
	@Override
	public void handleEditQuestionHTMLResponse(HttpServletRequest request) {
		preText = request.getParameter("preText");
		defaultText = request.getParameter("defaultText");
		postText = request.getParameter("postText");
		preferredResponse = request.getParameter("preferredResponse");
		String other_responses = request.getParameter("responses");
		responses.clear();
		for (String s : other_responses.split("\\r?\\n")) {
			if (!s.equals("")) responses.add(s);
		}
		saveQuestion();
	}
	
	@Override
	public String printUserResponse(String user_response) {
		return user_response;
	}
	
	@Override
	protected int gradeResponse(String response) {
		String lower = response.toLowerCase();
		if (lower.equalsIgnoreCase(preferredResponse)) return 1;
		if (responses.contains(lower)) return 1;
		return 0;
	}
	
	@Override
	protected String getCorrectResponse() {
		return preferredResponse;
	}

	@Override
	protected void saveQuestion() {
		deleteQuestion();
		db.update("INSERT INTO elements VALUES(NULL,'QUERY'," + questionID + ",'" + preText + "',0,0)");
		db.update("INSERT INTO elements VALUES(NULL,'QUERY'," + questionID + ",'" + postText + "',1,0)");
		db.update("INSERT INTO elements VALUES(NULL,'OPTION'," + questionID + ",'" + defaultText + "',0,0)");
		db.update("INSERT INTO elements VALUES(NULL,'ANSWER'," + questionID + ",'" + preferredResponse + "',0,1)");
		for (String s : responses) {
			db.update("INSERT INTO elements VALUES(NULL,'ANSWER'," + questionID + ",'" + s + "',0,0)");
		}
	}
}
