package quiz;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import project.MyDBInfo;

/**
 * The Multiple Choice question presents the user with a question and a bunch
 * of possible answers of which they can only select one (radiobuttons).  There
 * is no limit on the number of options, or even the number of correct options
 * but the user can only choose one.  The options can be labelled A.B.C. 1.2.3.
 * or not labeled (no label by default).
 * @author scott
 */
public class MultipleChoiceQuestion extends QuizQuestion {

	public static final String QUESTION_TYPE = "MULTIPLE CHOICE";
	
	private String question;
	private ArrayList<String> options;
	private ArrayList<Boolean> isCorrect;
	private boolean abcLabels, numLabels;
	
	protected MultipleChoiceQuestion() {
		this.question = "";
		this.options = new ArrayList<String>();
		options.add("answer option 1");
		options.add("answer option 2");
		options.add("answer option 3");
		this.isCorrect = new ArrayList<Boolean>();
		isCorrect.add(true);
		isCorrect.add(false);
		isCorrect.add(true);
		this.abcLabels = true;
		this.numLabels = false;
	}
	
	@Override
	protected void downloadElements() {
		options.clear();
		isCorrect.clear();
		abcLabels = false;
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery("SELECT * FROM elements where questionID=" + questionID);
			while (rs.next()) {
				String data = rs.getString("elementStr");
				String type = rs.getString("elementType");
				int order = rs.getInt("elementOrder");
				int correct = rs.getInt("isCorrect");
				
				// Parse the various types of question elements differently
				if (type.equals("QUERY")) {
					question = data;
				} else if (type.equals("OPTION")) {
					expandListsToSize(order);
					options.set(order, data);
					isCorrect.set(order, correct == 1);
				} else if (type.equals("ANSWER")) {
					if (data.equals("abcLabels")) abcLabels = true;
					if (data.equals("numLabels")) numLabels = true;
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
	}
	
	@Override
	public String displayQuestion() {
		return question;
	}
	
	@Override
	public String displayQuestionAndAnswers() {
		StringBuilder sb = new StringBuilder();
		sb.append("Question: ");
		sb.append(displayQuestion());
		sb.append("<br>Options:<br>");
		for (int i = 0; i < options.size(); i++) {
			if (abcLabels) sb.append(getABCLabel(i) + ") ");
			if (numLabels) sb.append(getNumLabel(i) + ") ");
			sb.append(options.get(i));
			if (isCorrect.get(i)) sb.append(" *correct*");
			sb.append("<br>");
		}
		return sb.toString();
	}
	
	@Override
	public String getQuestionHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append(question);
		sb.append("<br>");
		for (int i = 0; i < options.size(); i++) {
			if (abcLabels) sb.append(getABCLabel(i) + ") ");
			if (numLabels) sb.append(getNumLabel(i) + ") ");
			sb.append("<input type='radio' name='");
			sb.append(questionID);
			sb.append("' value='");
			sb.append(i);
			sb.append("'>");
			sb.append(options.get(i));
			sb.append("<br>");
		}
		return sb.toString();
	}
	
	@Override
	public String getEditQuestionHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append("Question: <input type='text' name='question' value='");
		sb.append(question);
		sb.append("'><br>");
		sb.append("Answer options (put each response on its own line):<br><textarea rows='10' cols='40' name='options'>");
		for (int i = 0; i < options.size(); i++) {
			sb.append(options.get(i));
			sb.append("\n");
		}
		sb.append("</textarea><br>");
		sb.append("Correct answers (put the line number, more than one correct answer is okay just separate them with commas):<br>");
		sb.append("<input type='text' name='lineNums' value='");
		for (int i = 0; i < isCorrect.size(); i++) {
			if (isCorrect.get(i)) {
				sb.append(i+1);
				sb.append(",");
			}
		}
		sb.append("'><br>Question Label Type: ");
		sb.append("<select name='labels'>");
		if (abcLabels) sb.append("<option value='abc' selected >ABCD Labels</option>");
		else sb.append("<option value='abc'>ABCD Labels</option>");
		if (numLabels) sb.append("<option value='num' selected >Number Labels</option>");
		else sb.append("<option value='num'>Number Labels</option>");
		if (!abcLabels && !numLabels) sb.append("<option value='no' selected >No Labels</option>");
		else sb.append("<option value='no'>No Labels</option>");
		sb.append("</select><br>");
		return sb.toString();
	}
	
	@Override
	public void handleEditQuestionHTMLResponse(HttpServletRequest request) {
		question = request.getParameter("question");
		String new_options = request.getParameter("options");
		options.clear();
		for (String s : new_options.split("\\r?\\n")) {
			if (!s.equals("")) options.add(s);
		}
		String new_correct = request.getParameter("lineNums");
		isCorrect.clear();
		expandListsToSize(options.size()-1);
		for (String s : new_correct.split(",")) {
			try {
				int i = Integer.parseInt(s);
				if (i-1 >= options.size()) throw new Exception();
				isCorrect.set(i-1, true);
			} catch (Exception ignored) {}
		}
		abcLabels = false;
		numLabels = false;
		String lbl = request.getParameter("labels");
		if (lbl.equals("abc")) abcLabels = true;
		if (lbl.equals("num")) numLabels = true;
		saveQuestion();
	}

	@Override
	public String printUserResponse(String user_response) {
		try {
			int i = Integer.parseInt(user_response);
			return options.get(i);
		} catch (Exception ignored) {}
		return "";
	}
	
	@Override
	public int gradeResponse(String response) {
		try {
			int optionNum = Integer.parseInt(response);
			if (isCorrect.get(optionNum)) return 1;
		} catch (Exception ignored) {}
		return 0;
	}

	@Override
	protected String getCorrectResponse() {
		String correct = "";
		for (int i = 0; i < isCorrect.size(); i++) {
			if (isCorrect.get(i)) {
				if (correct.equals("")) correct += options.get(i);
				else correct += " OR " + options.get(i);
			}
		}
		return correct;
	}
	
	@Override
	protected void saveQuestion() {
		deleteQuestion();
		db.update("INSERT INTO elements VALUES(NULL,'QUERY'," + questionID + ",'" + question + "',0,0)");
		for (int i = 0; i < options.size(); i++) {
			String opt = options.get(i);
			String ord = "" + i;
			String cor = isCorrect.get(i) ? "1" : "0";
			db.update("INSERT INTO elements VALUES(NULL,'OPTION'," + questionID + ",'" + opt + "'," + ord + "," + cor + ")");
		}
		if (abcLabels) db.update("INSERT INTO elements VALUES(NULL,'ANSWER'," + questionID + ",'abcLabels',0,1)");
		if (numLabels) db.update("INSERT INTO elements VALUES(NULL,'ANSWER'," + questionID + ",'numLabels',0,1)");
	}
	
	// HELPER METHODS
	
	private String getABCLabel(int optionNum) {
		return getABCLabelRecursive(optionNum, "");
	}
	
	private String getABCLabelRecursive(int optionNum, String cur) {
		if (optionNum < 0) return cur;
		String nextStr = "" + ((char) ('A' + optionNum%26)) + cur;
		int next = optionNum / 26 - 1;
		if (next >= 0) return getABCLabelRecursive(next, nextStr);
		return nextStr;
	}
	
	private String getNumLabel(int optionNum) {
		return "" + (optionNum + 1);
	}
	
	private void expandListsToSize(int pos) {
		if (options.size() <= pos) {
			for (int i = options.size(); i <= pos; i++) options.add(null);
		}
		if (isCorrect.size() <= pos) {
			for (int i = isCorrect.size(); i <= pos; i++) isCorrect.add(false);
		}
	}
}
