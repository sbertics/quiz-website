package quiz;

import java.util.*;
import java.sql.*;

import javax.servlet.http.HttpServletRequest;

import project.MyDBInfo;

/**
 * The Question Response question presents the user with a single text question
 * and accepts a text response (case ignored).  There is one preferred response,
 * but as many possible responses as you feel like adding :)
 */
public class QuestionResponseQuestion extends QuizQuestion {
	
	public static final String QUESTION_TYPE = "QUESTION RESPONSE";
	
	private String question;
	private String defaultText;
	private String preferredResponse;
	private Set<String> responses;
	
	protected QuestionResponseQuestion() {
		this.question = "";
		this.defaultText = "";
		this.preferredResponse = "";
		this.responses = new HashSet<String>();
	}
	
	@Override
	protected void downloadElements() {
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery("SELECT * FROM elements where questionID=" + questionID);
			while (rs.next()) {
				String data = rs.getString("elementStr");
				String type = rs.getString("elementType");
				int correct = rs.getInt("isCorrect");
				
				// Parse the various types of question elements differently
				if (type.equals("QUERY")) {
					question = data;
				} else if (type.equals("OPTION")) {
					defaultText = data;
				} else if (type.equals("ANSWER")) {
					if (correct == 1) preferredResponse = data;
					else responses.add(data);
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
	}

	@Override
	public String displayQuestion() {
		return question;
	}
	
	@Override
	public String displayQuestionAndAnswers() {
		StringBuilder sb = new StringBuilder();
		sb.append("Question: ");
		sb.append(displayQuestion());
		sb.append("<br>Default Response Text: ");
		sb.append(defaultText);
		sb.append("<br>Preferred Response: ");
		sb.append(preferredResponse);
		sb.append("<br>Other Responses:<br><ul>");
		for (String s : responses) {
			sb.append("<li>");
			sb.append(s);
			sb.append("</li>");
		}
		sb.append("</ul>");
		return sb.toString();
	}
	
	@Override
	public String getQuestionHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append(question);
		sb.append("<br>");
		sb.append("<input type='text' name='");
		sb.append(questionID);
		sb.append("' placeholder='");
		sb.append(defaultText);
		sb.append("'>");
		return sb.toString();
	}
	
	@Override
	public String getEditQuestionHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append("Question: <input type='text' name='question' value='");
		sb.append(question);
		sb.append("'><br>");
		sb.append("Default Response Text: <input type='text' name='defaultText' value='");
		sb.append(defaultText);
		sb.append("'><br>");
		sb.append("Preferred Response: <input type='text' name='preferredResponse' value='");
		sb.append(preferredResponse);
		sb.append("'><br>");
		sb.append("Other Responses (put each response on its own line):<br><textarea rows='10' cols='40' name='responses'>");
		for (String s : responses) {
			sb.append(s);
			sb.append("\n");
		}
		sb.append("</textarea>");
		return sb.toString();
	}
	
	@Override
	public void handleEditQuestionHTMLResponse(HttpServletRequest request) {
		question = request.getParameter("question");
		defaultText = request.getParameter("defaultText");
		preferredResponse = request.getParameter("preferredResponse");
		String other_responses = request.getParameter("responses");
		responses.clear();
		for (String s : other_responses.split("\\r?\\n")) {
			if (!s.equals("")) responses.add(s);
		}
		saveQuestion();
	}

	@Override
	public String printUserResponse(String user_response) {
		return user_response;
	}
	
	@Override
	protected int gradeResponse(String response) {
		String lower = response.toLowerCase();
		if (lower.equalsIgnoreCase(preferredResponse)) return 1;
		if (responses.contains(lower)) return 1;
		return 0;
	}

	@Override
	protected String getCorrectResponse() {
		return preferredResponse;
	}
	
	@Override
	protected void saveQuestion() {
		deleteQuestion();
		db.update("INSERT INTO elements VALUES(NULL,'QUERY'," + questionID + ",'" + question + "',0,0)");
		db.update("INSERT INTO elements VALUES(NULL,'OPTION'," + questionID + ",'" + defaultText + "',0,0)");
		db.update("INSERT INTO elements VALUES(NULL,'ANSWER'," + questionID + ",'" + preferredResponse + "',0,1)");
		for (String s : responses) {
			db.update("INSERT INTO elements VALUES(NULL,'ANSWER'," + questionID + ",'" + s + "',0,0)");
		}
	}
}
