package quiz;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import project.*;

/**
 * The Quiz Class represents the meta data and questions associated with
 * a particular quiz.  It also maintains times and scores when playing it.
 * 
 * IMPORTANT USAGE INFORMATION
 * ===========================
 * Generating the quiz object:
 *   (1) Quiz.createQuiz(db, userID);
 *   (2) Quiz.loadQuiz(quizID);
 * Add and delete questions from the quiz:
 *   (1) quiz_object.createQuestion(MultipleChoiceQuestion.QUESTION_TYPE);
 *   (2) quiz_object.deleteQuestion(4);
 *   (3) quiz_object.deleteQuiz(); // permanent!!
 * Take the quiz:
 *   (1) quiz_object.startQuiz();
 *   (2) quiz_object.getQuizHTML();
 *   (3) quiz_object.gradeQuestions(request);
 *   (4) quiz_object.stopQuiz();
 */
public class Quiz {

	////////////////////////////
	// STATIC CLASS FUNCTIONS //
	////////////////////////////

	public static final int DEFAULT_NUM_QUIZZES_TO_DISPLAY = 5;

	/**
	 * Returns an unordered html list of all quizzes in the entire website with links.
	 */
	public static String getAllQuizzes(Database db) {
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery("SELECT * FROM quizzes");
			while (rs.next()) {
				sb.append(QuizData.getListEntryHTML(rs.getInt("quizID"),rs.getString("name")));
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
		sb.append("</ul>");
		return sb.toString();
	}


	/**
	 * Returns an unordered html list for the most recently created quizzes.
	 */
	public static String getRecentlyCreatedQuizzes(Database db, int num_quizzes_to_display) {
		if (num_quizzes_to_display < 0) num_quizzes_to_display = DEFAULT_NUM_QUIZZES_TO_DISPLAY;
		String query = "SELECT * FROM quizzes ORDER BY createTime DESC LIMIT " + num_quizzes_to_display;
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				sb.append(QuizData.getListEntryHTML(rs.getInt("quizID"),rs.getString("name")));
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
		return sb.toString();
	}

	/**
	 * Returns the html for the most recently created quizzes.
	 */
	public static String getUsersRecentlyCreatedQuizzes(Database db, int userID, int num_quizzes_to_display) {
		if (num_quizzes_to_display < 0) num_quizzes_to_display = DEFAULT_NUM_QUIZZES_TO_DISPLAY;
		String query = "SELECT * FROM quizzes WHERE creatorID=" + userID + " ORDER BY createTime DESC LIMIT " + num_quizzes_to_display;
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				sb.append(QuizData.getListEntryHTML(rs.getInt("quizID"),rs.getString("name")));
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
		sb.append("</ul>");
		return sb.toString();
	}

	/**
	 * Creates the quiz and permanently stores its data in the database.
	 */
	public static Quiz createQuiz(Database db, int userID) {
		Quiz q = new Quiz(db);
		q.data = new QuizData(db);
		q.data.creatorID = userID;
		if (q.data.insertIntoDatabase() == false) return null;
		return q;
	}


	/**
	 * Loads the quiz from the database.
	 * @param quizID
	 * @return
	 */
	public static Quiz loadQuiz(Database db, int quizID) {
		Quiz q = new Quiz(db);
		q.data = new QuizData(db);
		q.data.quizID = quizID;
		if (q.downloadQuiz() == false) return null;
		if (q.downloadQuestions() == false) return null;
		return q;
	}

	//////////////////////////////
	// INSTANCE CLASS FUNCTIONS //
	//////////////////////////////

	private Database db;
	private QuizData data;
	private ArrayList<QuizQuestion> questions;
	
	private long startTime, endTime;
	private int score;
	private ArrayList<String> userResponses;

	/**
	 * Quiz constructor stores a reference to the database.
	 * @param db
	 */
	private Quiz(Database db) {
		this.db = db;
		questions = new ArrayList<QuizQuestion>();
		userResponses = new ArrayList<String>();
	}

	/**
	 * Downloads this quiz's information from the database and stores
	 * it locally.
	 */
	private boolean downloadQuiz() {
		String query = "SELECT * FROM quizzes WHERE quizID=" + data.quizID ;
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			if (rs.next()) {
				data.quizID = rs.getInt("quizID");
				data.creatorID = rs.getInt("creatorID");
				data.name = rs.getString("name");
				data.description = rs.getString("description");
				data.category = rs.getString("category");
				data.questionOrderScheme = rs.getInt("questionOrderScheme");
				data.pageDisplayFormat = rs.getInt("pageDisplayFormat");
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) { return false; }
		return true;
	}

	/**
	 * Downloads the questions from the database and stores them in the arraylist
	 * of questions.  The actually content of each question is downloaded by the
	 * QuizQuestion class.
	 * @return true on success, false on failure
	 */
	private boolean downloadQuestions() {

		try {
			String query = "SELECT * FROM questions WHERE quizID = " + data.quizID;
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {

				// The questionID, type, and order
				int questionID = rs.getInt("questionID");
				String questionType = rs.getString("questionType");
				int questionOrder = rs.getInt("questionOrder");

				// The question is inserted in the appropriate order into the arraylist
				QuizQuestion qq = loadQuestion(questionID, questionType);
				expandQuestionsToSize(questionOrder);
				questions.set(questionOrder, qq);
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * Expands the questions ArrayList to accommodate an element at position pos.
	 */
	private void expandQuestionsToSize(int pos) {
		if (questions.size() <= pos) {
			for (int i = questions.size(); i <= pos; i++) {
				questions.add(null);
			}
		}
	}

	/**
	 * Creates a question of the given type and adds it to the end of the quiz
	 * @param questionType
	 * @return
	 */
	public QuizQuestion createQuestion(String questionType) {
		QuizQuestion qq = instantiateQuestionOfType(questionType);
		if (qq == null) return null;
		qq.questionID = db.update("INSERT INTO questions VALUES(NULL," + qq.quizID + ",'" + qq.questionType + "'," + questions.size() + ")");
		if (qq.questionID == -1) return null;
		questions.add(qq);
		return qq;
	}

	/**
	 * Creates the QuizQuestion object of the appropriate type and downloads
	 * its data from the database.
	 */
	protected QuizQuestion loadQuestion(int questionID, String questionType) {
		QuizQuestion qq = instantiateQuestionOfType(questionType);
		if (qq == null) return null;
		qq.questionID = questionID;
		qq.downloadElements();
		return qq;
	}

	/**
	 * Creates an empty question object of the appropriate type.  No database interaction.
	 */
	private QuizQuestion instantiateQuestionOfType(String type) {

		// Initializes the question
		QuizQuestion qq = null;
		if (type.equals(QuestionResponseQuestion.QUESTION_TYPE)) {
			qq = new QuestionResponseQuestion();
		} else if (type.equals(FillInTheBlankQuestion.QUESTION_TYPE)){
			qq = new FillInTheBlankQuestion();
		} else if (type.equals(MultipleChoiceQuestion.QUESTION_TYPE)){
			qq = new MultipleChoiceQuestion();
		} else if (type.equals(PictureResponseQuestion.QUESTION_TYPE)){
			qq = new PictureResponseQuestion();
		}
		if (qq == null) return null;

		// Sets the protected variables for the question
		qq.db = db;
		qq.quizID = data.quizID;
		qq.questionType = type;
		return qq;
	}
	
	/**
	 * Deletes the nth question from the quiz, and removes it from the database.
	 * Has to update the question order for all the questions that come after
	 * it in the database.  Returns true on success, false on a variety of conditions:
	 *   (1) questionNum is not a valid question number
	 */
	public boolean deleteQuestion(int questionNum) {

		// Error check input
		if (questionNum < 0) return false;
		if (questionNum >= questions.size()) return false;

		// Delete the question from the database
		QuizQuestion byebye = questions.get(questionNum);
		byebye.deleteQuestion();
		db.update("DELETE FROM questions WHERE questionID=" + byebye.questionID);
		questions.remove(questionNum);

		// Update the other questions in the database
		for (int i = 0; i < questions.size(); i++) {
			QuizQuestion qq = questions.get(i);
			db.update("UPDATE questions SET questionOrder=" + i + " WHERE questionID=" + qq.questionID);
		}
		return true;
	}
	
	/**
	 * FULLY DELETES THE QUIZ FROM THE DATABASE.  PERMANENTLY.
	 */
	public void deleteQuiz() {
		for (int i = 0; i < questions.size(); i++) {
			QuizQuestion byebye = questions.get(i);
			byebye.deleteQuestion();
			db.update("DELETE FROM questions WHERE questionID=" + byebye.questionID);
		}
		data.deleteFromDatabase();
		data = null;
		questions.clear();
		questions = null;
	}
	
	//////////////////////////////
	//   TAKE QUIZ FUNCTIONS    //
	//////////////////////////////
	
	/**
	 * Records the time and zeroes the score at the beginning of the quiz
	 */
	public void startQuiz() {
		startTime = new Date().getTime();
		score = 0;
		if (data.questionOrderScheme == QuizData.QUESTION_ORDER_RANDOM){
			Collections.shuffle(questions) ;
		}
	}
	
	/**
	 * Returns the complete html code to be displayed when taking the quiz.
	 * Possible responses automatically use the html input tag, so make sure
	 * to surround this returned code and other question's returned code in
	 * an html form tag!  Returns empty string if the quiz is supposed to be
	 * displayed on multiple pages.
	 */
	public String getQuizHTML(int questionNum) {
		
		// For multiple page quizzes only return question html for the question that they requested
		if (data.pageDisplayFormat != QuizData.PAGE_FORMAT_SINGLE_PAGE) {
			if (questionNum < 0 || questionNum >= getNumQuestions())
				return "<a href='quiz.jsp'>An error has occured</a>";
			return questions.get(questionNum).getQuestionHTML();
		}
		
		// For single page quizzes return everything in a list
		StringBuilder sb = new StringBuilder();
		if (data.questionOrderScheme == QuizData.QUESTION_ORDER_NUMBERED) sb.append("<ol>");
		else sb.append("<ul>");
		
		for (int i = 0; i < getNumQuestions(); i++) {
			sb.append("<li>");
			sb.append(questions.get(i).getQuestionHTML());
			sb.append("</li>");
		}
		
		if (data.questionOrderScheme == QuizData.QUESTION_ORDER_NUMBERED) sb.append("</ol>");
		else sb.append("</ul>");
		return sb.toString();
	}
	
	/**
	 * This function grades all questions that are returned in the http request.
	 * Every question contains an input tag where the name attribute equals the
	 * questionID and the value equals the users response.  All such parameters are
	 * graded if found and a string is returned that can be displayed in the event
	 * of an immediately graded question.
	 */
	public String gradeQuestions(HttpServletRequest request) {
		String result = "";
		Enumeration<String> params = request.getParameterNames();
		while (params.hasMoreElements()) {
			String questionID = params.nextElement();
			String user_response = request.getParameter(questionID);
			result = gradeQuestion(questionID, user_response);
		}
		return result;
	}
	
	/**
	 * Helper function leveraged by gradeQuestions to grade an individual question.
	 * It stores the user's response for display on the results page, updates the
	 * score, and returns a message indicating whether or not the user answered the
	 * question correctly.
	 */
	private String gradeQuestion(String questionID, String user_response) {
		int intQuestionID;
		try {
			intQuestionID = Integer.parseInt(questionID);
		} catch (Exception e) { return ""; }
		
		// Find the question that the user just submitted
		for (int i = 0; i < questions.size(); i++) {
			QuizQuestion qq = questions.get(i);
			if (qq.questionID == intQuestionID) {
				
				// Add the user's response to a private list
				expandResponsesToSize(i);
				userResponses.set(i,user_response);
				
				// Update score and return message indicating correct or incorrect
				int result_score = qq.gradeResponse(user_response);
				score += result_score;
				if (result_score == 0) return "Incorrect.  The correct answer was: " + qq.getCorrectResponse();
				return "Correct!";
			}
		}
		return "";
	}
	
	/**
	 * Expands the responses ArrayList to accommodate an element at position pos.
	 */
	private void expandResponsesToSize(int pos) {
		if (userResponses.size() <= pos) {
			for (int i = userResponses.size(); i <= pos; i++) {
				userResponses.add(null);
			}
		}
	}
	
	/**
	 * Returns the correct response as a text string for the questionNum
	 * question in the quiz.
	 */
	public String getQuestionCorrectResponse(int questionNum) {
		if (questionNum < 0 || questionNum >= questions.size()) return "Error";
		return questions.get(questionNum).getCorrectResponse();
	}
	
	/**
	 * Records the end time and finalizes the score
	 * at the end of the quiz.
	 */
	public void endQuiz() {
		endTime = new Date().getTime();
	}
	
	/**
	 * Returns an HTML table containing the results of the quiz
	 */
	public String getResults() {
		StringBuilder sb = new StringBuilder();
		sb.append("<table>");
		sb.append("<tr>");
		sb.append("<td><h3>Question</h3></td>");
		sb.append("<td><h3>Your Response</h3></td>");
		sb.append("<td><h3>Result</h3></td>");
		sb.append("</tr>");
		for (int i = 0; i < questions.size(); i++) {
			QuizQuestion qq = questions.get(i);
			String user_response = userResponses.get(i);
			sb.append("<tr>");
			sb.append("<td>");
			sb.append(qq.displayQuestion());
			sb.append("</td>");
			sb.append("<td>");
			sb.append(qq.printUserResponse(user_response));
			sb.append("</td>");
			sb.append("<td>");
			int grade = qq.gradeResponse(user_response);
			if (grade > 0) sb.append("Correct! +" + grade);
			else sb.append("Incorrect.  The correct answer was: " + qq.getCorrectResponse());
			sb.append("</td>");
			sb.append("</tr>");
		}
		sb.append("</table>");
		return sb.toString();
	}

	//////////////////////////////
	//         GETTERS          //
	//////////////////////////////
	
	// GET QUIZ DATA
	public int getQuizID() { return data.quizID; }
	public int getCreatorID() { return data.creatorID; }
	public String getName() { return data.name; }
	public String getCategory() { return data.category; }
	public String getDescription() { return data.description; }
	public int getQuestionOrderScheme() { return data.questionOrderScheme; }
	public int getPageDisplayFormat() { return data.pageDisplayFormat; }
	
	// EDIT QUIZ DATA
	public String getEditQuizData() { return data.getEditQuizData(); }
	public void updateQuizData(HttpServletRequest request) { data.updateQuizData(request); }
	
	// GET SCORE TIMES ETC
	public int getScore() { return score; }
	public long getStartTime() { return startTime; }
	public long getEndTime() { return endTime; }
	
	// GET QUIZ QUESTIONS
	public int getNumQuestions() { return questions.size(); }
	public QuizQuestion getQuestion(int questionNum) {
		if (questionNum < 0) return null;
		if (questionNum >= questions.size()) return null;
		return questions.get(questionNum);
	}
	public int getQuestionID(int questionNum) {
		if (questionNum < 0) return -1;
		if (questionNum >= questions.size()) return -1;
		return questions.get(questionNum).questionID;
	}
}
