package quiz;

import java.sql.*;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import project.*;

/**
 * This Class stores data associated with the quiz (id, name, description, etc)
 * but not the questions.  The Quiz Class combines one QuizData object with a
 * list of QuizQuestion objects.
 */
public class QuizData {

	////////////////////////////
	// STATIC CLASS FUNCTIONS //
	////////////////////////////

	// question order scheme
	public static final int QUESTION_ORDER_NUMBERED = 0;
	public static final int QUESTION_ORDER_RANDOM = 1;
	private static final int NUM_QUESTION_ORDER_SCHEMES = 2;

	// page display format
	public static final int PAGE_FORMAT_SINGLE_PAGE = 0;
	public static final int PAGE_FORMAT_MULTI_PAGE = 1;
	public static final int PAGE_FORMAT_MULTI_PAGE_IMMEDIATE = 2;
	private static final int NUM_PAGE_FORMATS = 3;

	// categories
	public static final String CATEGORIES [] = {
		"Animals",
		"Arts",
		"Computers",
		"Current Events",
		"Entertainment",
		"Geography",
		"Government",
		"History",
		"Math & Science",
		"Money & Economics",
		"People",
		"Religion",
		"Sports",
		"Words",
		"Other"
	};

	/**
	 * Turns the questionOrderScheme integer into a textual representation for display purposes
	 */
	protected static String getQuestionOrderSchemeText(int scheme) {
		switch (scheme) {
		case 0: return "Numbered";
		case 1: return "Random";
		default: return "Unknown";
		}
	}

	/**
	 * Turns the pageDisplayFormat integer into a textual representation for display purposes
	 */
	protected static String getPageDisplayFormatText(int format) {
		switch (format) {
		case 0: return "All Questions on One Page";
		case 1: return "Each Question on Own Page";
		case 2: return "Each Question on Own Page and Answers Displayed Immediately";
		default: return "Unknown";
		}
	}
	
	/**
	 * Returns the html for the user to select a questionOrderScheme.
	 */
	public static String getQuestionOrderSchemeHTML(int selected) {
		StringBuilder sb = new StringBuilder();
		sb.append("<select name='questionOrderScheme'>");
		for (int i = 0; i < NUM_QUESTION_ORDER_SCHEMES; i++) {
			if (selected == i) {
				sb.append("<option selected value='");
			} else {
				sb.append("<option value='");
			}
			sb.append(i);
			sb.append("'>");
			sb.append(getQuestionOrderSchemeText(i));
			sb.append("</option>");
		}
		sb.append("</select>");
		return sb.toString();
	}
	
	/**
	 * Returns the html for the user to select a pageDisplayFormat.
	 * @param selected
	 * @return
	 */
	public static String getPageDisplayFormatHTML(int selected) {
		StringBuilder sb = new StringBuilder();
		sb.append("<select name='pageDisplayFormat'>");
		for (int i = 0; i < NUM_PAGE_FORMATS; i++) {
			if (selected == i) {
				sb.append("<option selected value='");
			} else {
				sb.append("<option value='");
			}
			sb.append(i);
			sb.append("'>");
			sb.append(getPageDisplayFormatText(i));
			sb.append("</option>");
		}
		sb.append("</select>");
		return sb.toString();
	}
	
	/**
	 * Returns the html for selecting a category (html select tag)
	 * with the category_selected option selected by default.
	 */
	public static String getCategoriesHTML(String category_selected) {
		StringBuilder sb = new StringBuilder();
		sb.append("<select name='category'>");
		for (int i = 0; i < CATEGORIES.length; i++) {
			if (category_selected.equalsIgnoreCase(CATEGORIES[i])) {
				sb.append("<option selected value='");
			} else {
				sb.append("<option value='");
			}
			sb.append(CATEGORIES[i]);
			sb.append("'>");
			sb.append(CATEGORIES[i]);
			sb.append("</option>");
		}
		sb.append("</select>");
		return sb.toString();
	}
	
	/**
	 * Returns an arraylist of the quiz data for every quiz on the site sorted with the given query
	 */
	public static ArrayList<QuizData> getAllQuizzes(Database db, String mysqlQuery) {
		ArrayList<QuizData> quizzes = new ArrayList<QuizData>();
		try {
			Statement stmt = db.getConnection().createStatement();
			stmt.executeQuery("USE " + MyDBInfo.MYSQL_DATABASE_NAME);
			ResultSet rs = stmt.executeQuery("SELECT * FROM quizzes ORDER BY createTime DESC");
			while (rs.next()) {
				QuizData qd = new QuizData(db);
				qd.quizID = rs.getInt("quizID");
				qd.creatorID = rs.getInt("creatorID");
				qd.name = rs.getString("name");
				qd.category = rs.getString("category");
				qd.description = rs.getString("description");
				qd.questionOrderScheme = rs.getInt("questionOrderScheme");
				qd.pageDisplayFormat = rs.getInt("pageDisplayFormat");
				quizzes.add(qd);
			}
			rs.close();
			stmt.close();
		} catch (Exception ignored) {}
		return quizzes;
	}

	// Turns the quizID/name pair into a list entry <li> for display purposes
	protected static String getListEntryHTML(int quizID, String name) {
		return "<li><a href='quiz.jsp?id=" + quizID + "'>" + name + "</a></li>";
	}

	//////////////////////////////
	// INSTANCE CLASS FUNCTIONS //
	//////////////////////////////

	private Database db;
	protected int quizID, creatorID, questionOrderScheme, pageDisplayFormat;
	protected String name, category, description;

	protected QuizData(Database db) {
		this.db = db;
		name = "Your Quiz";
		category = "Other";
		description = "Your Quiz Description";
		questionOrderScheme = QuizData.QUESTION_ORDER_NUMBERED;
		pageDisplayFormat = QuizData.PAGE_FORMAT_SINGLE_PAGE;
	}

	protected boolean insertIntoDatabase() {
		quizID = db.update(
				"INSERT INTO quizzes VALUES(NULL," + creatorID + ",'" + name + "','" + category + "','" + description + "',"+ questionOrderScheme + "," + pageDisplayFormat + ",NULL)");
		if (quizID == -1) return false;
		return true;
	}

	protected void deleteFromDatabase() {
		db.update("DELETE FROM quizzes WHERE quizID=" + quizID);
	}

	/**
	 * Returns the html with input boxes to display in a form when editing
	 * the quiz data.  The resulting request can be passed into updateQuizData
	 * for processing.
	 */
	protected String getEditQuizData() {
		StringBuilder sb = new StringBuilder();
		sb.append("<ul>");
		sb.append("<li>Name: <input type='text' name='name' value='");
		sb.append(name);
		sb.append("'></li>");
		sb.append("<li>Category: ");
		sb.append(QuizData.getCategoriesHTML(category));
		sb.append("</li>");
		sb.append("<li>Description: <textarea cols='40' rows='10' name='description'>");
		sb.append(description);
		sb.append("</textarea>");
		sb.append("</li>");
		sb.append("<li>Question Order Scheme: ");
		sb.append(QuizData.getQuestionOrderSchemeHTML(questionOrderScheme));
		sb.append("</li>");
		sb.append("<li>Page Display Format: ");
		sb.append(QuizData.getPageDisplayFormatHTML(pageDisplayFormat));
		sb.append("</li>");
		sb.append("</ul>");
		return sb.toString();
	}

	/**
	 * getEditQuizData() produces a bunch of input tags that you should
	 * put into a form.  That form generates a HttpRequest that should
	 * then be passed into this function, updateQuizData, so the data
	 * can be stored and pushed to the database.
	 */
	protected void updateQuizData(HttpServletRequest request) {
		name = request.getParameter("name");
		db.update("UPDATE quizzes SET name='" + name + "' WHERE quizID=" + quizID);
		category = request.getParameter("category");
		db.update("UPDATE quizzes SET category='" + category + "' WHERE quizID=" + quizID);
		description = request.getParameter("description");
		db.update("UPDATE quizzes SET description='" + description + "' WHERE quizID = " + quizID);
		try {
			questionOrderScheme = Integer.parseInt(request.getParameter("questionOrderScheme"));
			db.update("UPDATE quizzes SET questionOrderScheme=" + questionOrderScheme + " WHERE quizID = " + quizID);
		} catch (Exception ignored) {}
		try {
			pageDisplayFormat = Integer.parseInt(request.getParameter("pageDisplayFormat"));
			db.update("UPDATE quizzes SET pageDisplayFormat=" + pageDisplayFormat + " WHERE quizID = " + quizID);
		} catch (Exception ignored) {}
	}

	// PUBLIC GETTERS
	public int getQuizID() { return quizID; }
	public int getCreatorID() { return creatorID; }
	public int getQuestionOrderScheme() { return questionOrderScheme; }
	public int getPageDisplayFormat() { return pageDisplayFormat; }
	public String getQuestionOrderSchemeText() { return QuizData.getQuestionOrderSchemeText(questionOrderScheme); }
	public String getPageDisplayFormatText() { return QuizData.getPageDisplayFormatText(pageDisplayFormat); }
	public String getName() { return name; }
	public String getCategory() { return category; }
	public String getDescription() { return description; }
	public String getListEntryHTML() { return QuizData.getListEntryHTML(quizID, name); }


}
