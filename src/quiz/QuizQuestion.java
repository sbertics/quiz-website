package quiz;

import javax.servlet.http.HttpServletRequest;

import project.Database;

/**
 * The QuizQuestion Class stores the data used to represent a particular question
 * and verify it's response.  It has specifically been written to allow the client
 * to do the following three actions as easily as possible:
 *   (1) Load a question into a quiz from the database
 *   (2) Create a question and add it to a quiz
 *   (3) Edit and update an existing question
 */
public abstract class QuizQuestion {
	
	////////////////////////////
	// STATIC CLASS FUNCTIONS //
	////////////////////////////
	
	/**
	 * Call this function to obtain the html to select a question
	 * type.
	 */
	public static String getAddQuestionHTML() {
		StringBuilder sb = new StringBuilder();
		sb.append("<select name='addQuestion'>");
		sb.append("<option>");
		sb.append(QuestionResponseQuestion.QUESTION_TYPE);
		sb.append("</option>");
		sb.append("<option>");
		sb.append(FillInTheBlankQuestion.QUESTION_TYPE);
		sb.append("</option>");
		sb.append("<option>");
		sb.append(MultipleChoiceQuestion.QUESTION_TYPE);
		sb.append("</option>");
		sb.append("<option>");
		sb.append(PictureResponseQuestion.QUESTION_TYPE);
		sb.append("</option>");
		sb.append("</select>");
		return sb.toString();
	}
	
	/**
	 * Call this function to handle the result from the above call
	 * to getAddQuestionHTML and determine which type of question
	 * the user selected.
	 */
	public static String getAddQuestionHTMLResponse(HttpServletRequest request) {
		String result = request.getParameter("addQuestion");
		if (result == null) return "";
		if (result.equals(QuestionResponseQuestion.QUESTION_TYPE)) return result;
		if (result.equals(FillInTheBlankQuestion.QUESTION_TYPE)) return result;
		if (result.equals(MultipleChoiceQuestion.QUESTION_TYPE)) return result;
		if (result.equals(PictureResponseQuestion.QUESTION_TYPE)) return result;
		return "";
	}
	
	//////////////////////////////
	// INSTANCE CLASS FUNCTIONS //
	//////////////////////////////
	
	protected Database db;
	protected int quizID;
	protected int questionID;
	protected String questionType;
	
	/**
	 * Returns only the question or relevant information to understand the question.
	 * Example return value:
	 *   What is the capital of California?
	 */
	public abstract String displayQuestion();
	
	/**
	 * Returns the entire question and all options/answers without any input tags,
	 * so it is non-interactive.  Only for display purposes, typically for the creator
	 * of the quiz to see his or her work.
	 * 
	 * Example return value:
	 *   What is the capital of California?<br>
	 *   A) San Francisco<br>
	 *   B) Los Angeles<br>
	 *   C) Sacramento *answer*<br>
	 *   D) Stanfurd<br>
	 */
	public abstract String displayQuestionAndAnswers();
	
	/**
	 * Returns the complete html code to be displayed when taking the quiz.
	 * Possible responses automatically use the html input tag, so make sure
	 * to surround this returned code and other question's returned code in
	 * an html form tag!
	 * 
	 * Example return value:
	 *   <img src="..." /><br>
	 *   This picture is of a:<br>
	 *   <input type="text" name="questionID" value="defaultText" /><br>
	 *   
	 * The name attribute of the each input tag for a given question must be the
	 * questionID.  The value attribute will contain the string that the client
	 * can pass into verifyResponse to see if the user answered the question
	 * correctly.
	 */
	public abstract String getQuestionHTML();
	
	/**
	 * Returns the html code to be displayed when editing the question.
	 * Example return value:
	 *   <input type="hidden" name="questionID" value="12345">
	 *   ......
	 */
	public abstract String getEditQuestionHTML();
	
	/**
	 * This function understands what to do with when the user fills out the edit
	 * question form and presses submit.  getEditQuestionHTML() names a bunch of
	 * input parameters and this function, handleEditQuestionHTMLResponse, uses
	 * those names to update the question and store the result in the database.
	 */
	public abstract void handleEditQuestionHTMLResponse(HttpServletRequest request);
	
	/**
	 * Given the response passed in from the input tag, return a printable version
	 * of that response to be displayed in a quiz results table.  For example, in a
	 * question response question, the user_response may be "here is my answer"
	 * so you can just return "here is my answer", but in a multiple choice question,
	 * the user_response may be '1' and you have to return options.get(1) which actually
	 * has the text the user was referring to.
	 */
	public abstract String printUserResponse(String user_response);
	
	/**
	 * Pass to this function the response the user entered aka the value attribute
	 * of the input tag for the question.  It returns the number of points the user
	 * scored on the question, or zero if the user got the question incorrect
	 */
	protected abstract int gradeResponse(String response);
	
	/**
	 * Returns a string representation of the correct response.  In cases where multiple
	 * responses are accepted, this function may only return the preferred response.
	 */
	protected abstract String getCorrectResponse();
	
	/**
	 * Downloads the quiz elements from the elements table in the database and populates
	 * the quiz with the appropriate information.
	 */
	protected abstract void downloadElements();
	
	/**
	 * This function removes all entries for this question from the elements table
	 * and then prints the current data to the element table.  The client calls
	 * saveQuiz to leverage this function.
	 */
	protected abstract void saveQuestion();
	
	/**
	 * This function removes all entries for this question from the elements table.
	 */
	protected void deleteQuestion() { db.update("DELETE FROM elements WHERE questionID=" + questionID); }
}
