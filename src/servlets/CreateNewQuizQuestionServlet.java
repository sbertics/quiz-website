package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quiz.*;

/**
 * Servlet implementation class CreateNewQuizQuestionServlet
 */
@WebServlet("/CreateNewQuizQuestionServlet")
public class CreateNewQuizQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public CreateNewQuizQuestionServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String questionType = QuizQuestion.getAddQuestionHTMLResponse(request);
		if (questionType.equals("")) {
			response.sendRedirect("edit-quiz.jsp");
		} else {
			Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
			quiz.createQuestion(questionType);
			request.getSession().setAttribute("q", quiz.getNumQuestions() - 1);
			response.sendRedirect("edit-question.jsp");
		}
	}

}
