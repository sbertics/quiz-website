package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import project.*;
import quiz.Quiz;

/**
 * Servlet implementation class CreateNewQuizServlet
 */
@WebServlet("/CreateNewQuizServlet")
public class CreateNewQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public CreateNewQuizServlet() {
        super();
    }
    
	/**
	 * When the user clicks the create new quiz button on their home.jsp page, they are sent here. 
	 * A quiz is automatically created with a few defaults and they are forwarded to edit-quiz.jsp
	 * where they can save the blank quiz, edit the quiz, or delete the quiz.
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Database db = (Database) request.getServletContext().getAttribute("database");
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		Quiz new_quiz = Quiz.createQuiz(db, user.getUserID());
		if (new_quiz == null) {
			session.setAttribute("message", "Error creating the quiz...");
			response.sendRedirect("home.jsp");
		} else {
			request.getSession().setAttribute("quiz", new_quiz);
			response.sendRedirect("edit-quiz.jsp");
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { }

}
