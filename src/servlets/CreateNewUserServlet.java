package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import project.Database;
import project.User;

/**
 * Servlet implementation class CreateNewUserServlet
 */
@WebServlet("/CreateNewUserServlet")
public class CreateNewUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreateNewUserServlet() {
        super();
    }

	/**
	 * GET request to CreateNewUserServlet automatically forwards to
	 * create-user.jsp
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { }

	/**
	 * POST to CreateNewUserServlet results in one of the following:
	 *   (1) User created, logged in, told successfully logged in
	 *   (2) Username in use, forwarded to create-user.jsp, told username in use
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Obtain the necessary variables
		HttpSession session = request.getSession(true);
		Database db = (Database) session.getServletContext().getAttribute("database");
		String username = request.getParameter("username");
		String password =  request.getParameter("password");
		
		// Try to load the user
		User new_user = User.createNewUser(db, username, password);
		session.setAttribute("user", new_user);
		if (null == new_user) {
			session.setAttribute("message", "The username, " + username + ", is already taken!");
			response.sendRedirect("create-user.jsp");
		} else {
			response.sendRedirect("home.jsp");
		}
		
	}

}
