package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import quiz.*;

/**
 * Servlet implementation class EditQuizDataServlet
 */
@WebServlet("/EditQuizDataServlet")
public class EditQuizDataServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public EditQuizDataServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { }

	/**
	 * Forwards the data received from edit-quiz-data.jsp back
	 * to the QuizData class to update the database.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
		quiz.updateQuizData(request);
		response.sendRedirect("edit-quiz.jsp");
	}

}
