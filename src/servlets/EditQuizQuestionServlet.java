package servlets;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quiz.Quiz;

/**
 * Servlet implementation class EditQuizQuestionServlet
 */
@WebServlet("/EditQuizQuestionServlet")
public class EditQuizQuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public EditQuizQuestionServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Obtain the session variables that we will be using
		Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
		
		// Find and edit or delete the appropriate question
		try {
			Enumeration<String> params = request.getParameterNames();
			if (params.hasMoreElements()) {
				String name = params.nextElement();
				String value = request.getParameter(name);
				int num = Integer.parseInt(name);
				request.getSession().setAttribute("q", num);
				if (value.equals("Edit")) {
					response.sendRedirect("edit-question.jsp");
				} else {
					quiz.deleteQuestion(num);
					request.getSession().setAttribute("message", "Question " + num + " Removed");
					response.sendRedirect("edit-quiz.jsp");
				}
			} else { throw new Exception(); }
		} catch (Exception caught) {
			request.getSession().setAttribute("message", "An error occurred!");
			response.sendRedirect("edit-quiz.jsp");
			
		}
	}
}
