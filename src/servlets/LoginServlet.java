package servlets;

import java.io.IOException;

import javax.servlet.http.*;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import project.Database;
import project.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

    public LoginServlet() {
        super();
    }

	/**
	 * GET requests for the LoginServlet are automatically forwarded
	 * to the login.jsp webpage if no user is logged in or home.jsp
	 * if the user is logged in
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { }

	/**
	 * POST requests to the LoginServlet forward to the login.jsp webpage
	 * if the username/password could did not verify.  The user is forwarded
	 * to home.jsp if they logged in successfully.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Obtain the necessary variables
		HttpSession session = request.getSession(true);
		Database db = (Database) session.getServletContext().getAttribute("database");
		String username = request.getParameter("username");
		String password =  request.getParameter("password");
		
		// Try to load the user
		User new_user = User.createUser(db, username, password);
		session.setAttribute("user", new_user);
		if (new_user == null)  {
			session.setAttribute("message", "Incorrect username or password!");
			response.sendRedirect("login.jsp");
		} else {
			response.sendRedirect("home.jsp");
		}
	}

}
