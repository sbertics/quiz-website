package servlets;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import project.Database;
/**
 * This listener is initialized when the server starts up and destroyed
 * when the server goes down.  It maintains a single database connection
 * under the attribute name "database" that all classes on the site
 * reference when contacting the database.
 */
@WebListener
public class ServerListener implements ServletContextListener {

    public ServerListener() { }

    public void contextInitialized(ServletContextEvent e) {
    	Database db = new Database();
    	e.getServletContext().setAttribute("database", db);
    }

    public void contextDestroyed(ServletContextEvent e) {
    	Database db = (Database) e.getServletContext().getAttribute("database");
    	db.closeConnection();
    	e.getServletContext().removeAttribute("database");
    }
	
}
