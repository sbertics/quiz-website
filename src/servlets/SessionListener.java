package servlets;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class SessionListener.  This listener runs for the lifecycle
 * of the client's session with the server.  All variables associated with a user's progression through a
 * series of pages on this site are maintained here.  The session maintains three variables, see session_variables.txt
 * in the doc folder.
 */
@WebListener
public class SessionListener implements HttpSessionListener {

    public SessionListener() { }

    public void sessionCreated(HttpSessionEvent e) {
    	HttpSession session = e.getSession();
    	session.setAttribute("user", null);
    	session.setAttribute("quiz", null);
    	session.setAttribute("q", null);
    }

    public void sessionDestroyed(HttpSessionEvent e) {
    	HttpSession session = e.getSession();
    	session.removeAttribute("user");
    	session.removeAttribute("quiz");
    	session.removeAttribute("q");
    }
	
}
