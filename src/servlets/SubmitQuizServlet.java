package servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import quiz.Quiz;
import quiz.QuizData;

/**
 * Servlet implementation class SubmitQuizServlet
 */
@WebServlet("/SubmitQuizServlet")
public class SubmitQuizServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public SubmitQuizServlet() {
        super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { }
	
	/**
	 * After the user has answer a question or questions, the results come here.
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// Obtain the variables from the session and parameters that we need
		Quiz quiz = (Quiz) request.getSession().getAttribute("quiz");
		int questionNum = (Integer) request.getSession().getAttribute("q");
		
		// Depending on how many questions were were expecting to process
		// at the current time, we will take a different action
		switch (quiz.getPageDisplayFormat()) {
		
		case QuizData.PAGE_FORMAT_SINGLE_PAGE:
			quiz.endQuiz();
			quiz.gradeQuestions(request);
			response.sendRedirect("results.jsp");
			break;
		
		case QuizData.PAGE_FORMAT_MULTI_PAGE:
			if (questionNum == quiz.getNumQuestions()-1) {
				quiz.endQuiz();
				quiz.gradeQuestions(request);
				response.sendRedirect("results.jsp");
			} else {
				quiz.gradeQuestions(request);
				request.getSession().setAttribute("q", questionNum + 1);
				response.sendRedirect("take-quiz.jsp");
			}
			break;
		
		case QuizData.PAGE_FORMAT_MULTI_PAGE_IMMEDIATE:
			String result = quiz.gradeQuestions(request);
			request.getSession().setAttribute("message", result);
			
			if (questionNum == quiz.getNumQuestions()-1) {
				quiz.endQuiz();
				response.sendRedirect("results.jsp");
			} else {
				request.getSession().setAttribute("q", questionNum+1);
				response.sendRedirect("take-quiz.jsp");
			}
			break;
		
		default: break;
		}
	}
}
